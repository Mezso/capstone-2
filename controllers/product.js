const Product = require("../models/product");

// Get all active product
module.exports.retrieveAll = (reqBody) => {
	return Product.find({isActive:true}).then(result =>{
		return result
	})
}
// Get specific Product with ID
module.exports.retrieveOne = (reqParams) =>{
	return Product.findById(reqParams.ProductId).then(result =>{
		return result
	})
}



// Creation of product
module.exports.createProduct = (reqBody) =>{
	let newProduct = new Product({
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((course,error) =>{
		if(error){
			return 'Invalid Product'
		}
		else {
			return 'Product are created'
		}
	})
}
// Update the product
module.exports.updateProduct = (reqParams, reqBody) =>{

	let updatedProduct = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error) =>{
		if(error){
			return 'failed to updated'
		} 
		else {
			return 'Updated successfully'
		}
	})
}
// Archieve the product
module.exports.archieveProduct = (reqParams,reqBody) =>{

	let archievedProduct = {
		isActive : reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId,archievedProduct).then((product,error) =>{
		if(error){
			return 'failed to archieve'
		} 
		else {
			return 'Product archieved'
		}
	})

}