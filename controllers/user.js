const User = require('../models/user')
const Product = require('../models/product')
const bcrypt = require('bcrypt')
const auth =require('../auth')

module.exports.registerUser = (reqBody) =>{
	let newUser = new User ({
		email : reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return 'Invalid input,try again'
		}
		else{
			return 'you are registered'
		}
	})
}

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return 'Invalid Credential'
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){

				return {access:auth.createAccessToken(result)}

			} else{
				return 'Invalid Credential'
			}
		}
	}) 
}

module.exports.createOrder = async (data) =>{

	let isUserOrder = await User.findById(data.userId).then(user =>{
		user.Order.push({
			productId: data.productId,
			totalAmount: data.totalAmount
		})

		return user.save().then((user,error)=>{
			if(error){
				return 'Order failed'
			}
			else {
				return 'Order Success'
			}
		})
	})

	let isProductOrder = await Product.findById(data.productId).then( product =>{
		product.Order.push({
			userId: data.userId,
			totalAmount : data.totalAmount
		})
		return product.save().then((product,error)=>{
			if(error){
				return 'Order failed'
			}
			else {
				return 'Order Success'
			}
		})


	})
	if(isUserOrder && isProductOrder){
			return 'Order Success'
		} else {
			return 'Order failed'
		}
	
}

module.exports.retrieveUserOrder = async (user, reqBody) =>{
	if(user.id == reqBody.id){
		return User.findOne({_id:reqBody.id}).then(result =>{
			if(result == null){
				return 'no order'
			}
			else {
				return result.Order
			}
		})
	} 
	else {
		return 'invalid Credential'
	}
	
}

module.exports.getAllOrder = () => {
	return User.find({}).then(result =>{
		return result.Order
	})
}

module.exports.setAsAdmin = (reqParams,reqBody) =>{
	let setAsAdmin = {
		isAdmin : reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId,setAsAdmin).then((user,error)=>{
		if(error){
			return 'failed to update'
		}
		else {
			return 'status update'
		}
	})
}

module.exports.getAllUser = () =>{
	return User.find({}).then(result =>{
		return result
	})
}