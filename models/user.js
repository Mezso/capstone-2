const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	email:{
		type: String,
		required :[true,'Name is required']
	},
	password:{
		type : String,
		required :[true,'Password is required']
	},
	isAdmin:{
		type : Boolean,
		default : false
	},
	Order:[
	{
		productId: {
			type :String,
			required: [true,'product Id is required']
		},
		totalAmount :{
			type: Number,
			required :[true,'Your order needed']
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		}
	}
	]
})

module.exports = mongoose.model('User',userSchema)